package com.sintad.tipocontribuyentemicroservice.infraestructure.adapter.entity;

import com.sintad.tipocontribuyentemicroservice.domain.model.TipoContribuyente;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 *
 * @author roberth
 */
@Table(value = "tb_tipo_contribuyente")
@Getter
@Setter
@Builder
public class TipoContribuyenteEntity {

    @Id
    private Long idTipoContribuyente;
    private String nombre;
    private boolean estado;
    
    public TipoContribuyente toDomain(TipoContribuyenteEntity entity) {
        return TipoContribuyente.builder()
                .idTipoContribuyente(entity.getIdTipoContribuyente())
                .nombre(entity.getNombre())
                .estado(entity.isEstado())
                .build();
    }

    public TipoContribuyenteEntity fromDomain(TipoContribuyente model) {
        return TipoContribuyenteEntity.builder()
                .idTipoContribuyente(model.getIdTipoContribuyente())
                .nombre(model.getNombre())
                .estado(model.isEstado())
                .build();
    }
}
