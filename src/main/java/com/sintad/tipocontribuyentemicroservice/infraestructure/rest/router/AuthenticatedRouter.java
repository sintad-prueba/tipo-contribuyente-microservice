package com.sintad.tipocontribuyentemicroservice.infraestructure.rest.router;

import com.sintad.tipocontribuyentemicroservice.infraestructure.rest.handler.TipoContribuyenteHandler;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 *
 * @author roberth
 */
@Configuration
public class AuthenticatedRouter {
    private static String path = "/api/auth";  

    @Bean
    RouterFunction<ServerResponse> routerAut(TipoContribuyenteHandler handler) {
        return RouterFunctions.route()
                .GET(path, handler::findAll1)
                .build();
    }
}
