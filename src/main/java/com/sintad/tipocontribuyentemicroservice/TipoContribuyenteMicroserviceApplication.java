package com.sintad.tipocontribuyentemicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TipoContribuyenteMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TipoContribuyenteMicroserviceApplication.class, args);
	}

}
