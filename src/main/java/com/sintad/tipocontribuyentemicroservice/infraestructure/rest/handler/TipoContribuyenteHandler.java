package com.sintad.tipocontribuyentemicroservice.infraestructure.rest.handler;

import com.sintad.tipocontribuyentemicroservice.aplication.service.TipoContribuyenteService;
import com.sintad.tipocontribuyentemicroservice.domain.model.TipoContribuyente;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
@Component
@AllArgsConstructor
public class TipoContribuyenteHandler {
    
     private final MediaType typeJson = MediaType.APPLICATION_JSON;
     
     TipoContribuyenteService service;
     
     public Mono<ServerResponse> save(ServerRequest request){
        Mono<TipoContribuyente> tipoDocumento = request.bodyToMono(TipoContribuyente.class);
        
        return tipoDocumento.flatMap(model -> ServerResponse.ok()
                .contentType(typeJson)
                .body(service.save(model), TipoContribuyente.class)
        );
    }
    
    public Mono<ServerResponse> update(ServerRequest request){
        Mono<TipoContribuyente> tipoDocumento = request.bodyToMono(TipoContribuyente.class);
        
        return tipoDocumento.flatMap(model -> ServerResponse.ok()
                .contentType(typeJson)
                .body(service.update(model), TipoContribuyente.class)
        );
    }
    
    public Mono<ServerResponse> deleteById(ServerRequest request){
        Long id = Long.valueOf(request.pathVariable("id"));
        
        return ServerResponse.ok()
                .contentType(typeJson)
                .body(service.deleteById(id), TipoContribuyente.class);
    }
    
    public Mono<ServerResponse> findAll(ServerRequest request){
        return ServerResponse.ok()
                .contentType(typeJson)
                .body(service.findAll(), TipoContribuyente.class);
    }
    
     public Mono<ServerResponse> findAll1(ServerRequest request){
        return ServerResponse.ok()
                .contentType(typeJson)
                .body(service.findAll(), TipoContribuyente.class);
    }
    
}
