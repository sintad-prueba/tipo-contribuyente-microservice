package com.sintad.tipocontribuyentemicroservice.aplication.usecase;

import com.sintad.tipocontribuyentemicroservice.domain.model.TipoContribuyente;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
public interface TipoContribuyenteUseCase {

    Flux<TipoContribuyente> findAll();
    Mono<TipoContribuyente> save(TipoContribuyente model);
    Mono<TipoContribuyente> update(TipoContribuyente model);
    Mono<Void> deleteById(Long id);
}
