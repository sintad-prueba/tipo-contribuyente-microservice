package com.sintad.tipocontribuyentemicroservice.infraestructure.adapter;

import com.sintad.tipocontribuyentemicroservice.domain.model.TipoContribuyente;
import com.sintad.tipocontribuyentemicroservice.domain.port.TipoContribuyentePersistencePort;
import com.sintad.tipocontribuyentemicroservice.infraestructure.adapter.entity.TipoContribuyenteEntity;
import com.sintad.tipocontribuyentemicroservice.infraestructure.adapter.repository.TipoContribuyenteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
@Service
@AllArgsConstructor
public class TipoContribuyenteR2DBCAdapter implements TipoContribuyentePersistencePort {
    
    TipoContribuyenteRepository repository;

    @Override
    public Mono<TipoContribuyente> save(TipoContribuyente model) {
         return repository.save(TipoContribuyenteEntity.builder().build().fromDomain(model))
                .map(entity -> entity.toDomain(entity));
    }

    @Override
    public Mono<TipoContribuyente> update(TipoContribuyente model) {
        return repository.save(TipoContribuyenteEntity.builder().build().fromDomain(model))
                .map(entity -> entity.toDomain(entity));
    }

    @Override
    public Mono<Void> deleteById(Long id) {
        return repository.deleteById(id);
    }

    @Override
    public Flux<TipoContribuyente> findAll() {
         return repository.findAll()
                 .map(entity -> entity.toDomain(entity));
               
    }
    
}
