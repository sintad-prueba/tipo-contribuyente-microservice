package com.sintad.tipocontribuyentemicroservice.aplication.service;

import com.sintad.tipocontribuyentemicroservice.aplication.usecase.TipoContribuyenteUseCase;
import com.sintad.tipocontribuyentemicroservice.domain.model.TipoContribuyente;
import com.sintad.tipocontribuyentemicroservice.domain.port.TipoContribuyentePersistencePort;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class TipoContribuyenteService implements TipoContribuyenteUseCase {
    
    TipoContribuyentePersistencePort persistence;

    @Override
    public Flux<TipoContribuyente> findAll() {
        return persistence.findAll();
    }

    @Override
    public Mono<TipoContribuyente> save(TipoContribuyente model) {
        return persistence.save(model);
    }

    @Override
    public Mono<TipoContribuyente> update(TipoContribuyente model) {
        return persistence.update(model);
    }

    @Override
    public Mono<Void> deleteById(Long id) {
        return persistence.deleteById(id);
    }
    
}
