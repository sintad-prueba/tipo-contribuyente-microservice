
package com.sintad.tipocontribuyentemicroservice.infraestructure.adapter.repository;

import com.sintad.tipocontribuyentemicroservice.domain.model.TipoContribuyente;
import com.sintad.tipocontribuyentemicroservice.infraestructure.adapter.entity.TipoContribuyenteEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author roberth
 */
@Repository
public interface TipoContribuyenteRepository extends ReactiveCrudRepository<TipoContribuyenteEntity, Long>{
    
}
