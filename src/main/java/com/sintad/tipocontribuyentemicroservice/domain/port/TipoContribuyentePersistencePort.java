package com.sintad.tipocontribuyentemicroservice.domain.port;

import com.sintad.tipocontribuyentemicroservice.domain.model.TipoContribuyente;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
public interface TipoContribuyentePersistencePort {
    Mono<TipoContribuyente> save (TipoContribuyente model);
    Mono<TipoContribuyente> update (TipoContribuyente model);
    Mono<Void> deleteById (Long id);
    Flux<TipoContribuyente> findAll();
}
