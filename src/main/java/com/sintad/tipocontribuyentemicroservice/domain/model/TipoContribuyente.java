package com.sintad.tipocontribuyentemicroservice.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 *
 * @author roberth
 */
@Getter
@Setter
@Builder
public class TipoContribuyente {
    private Long idTipoContribuyente;
    private String nombre;
    private boolean estado;
}
